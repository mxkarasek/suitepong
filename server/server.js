require('./env.js');
const express = require('express');
const cron = require('node-cron');
const errorHandler = require('errorhandler');
const bodyParser = require('body-parser');
const path = require('path');
const https = require('https');
const favicon = require('serve-favicon');
const letsencrypt = require('../app/util/letsencrypt.js');
const booking = require("../app/booking/booking.js");
const login = require("../app/user/login.js");
const time = require("../app/util/time.js");
const purger = require('../app/booking/purger.js');

const logger = require('winston');
logger.level = process.env.DEBUG_LEVEL;

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(favicon(__dirname + '/../public/favicon.ico'));
app.use(express.static(path.join(__dirname, '/../public')));
app.use('/.well-known/', letsencrypt);
app.use('/v1/booking', booking);
app.use('/v1/time', time);
app.use('/v1/login', login);

if (app.get('env') === 'development') {
	app.use(errorHandler());
}

app.set('port', process.env.PORT || 8080);
app.listen(app.get('port'), function () {
	logger.log("SuitePong server started...");
});

cron.schedule('10 * * * * *', function () {
	logger.log('debug', 'Executing purger');
	purger.purge(function (err) {
		if (err) logger.log('error', err);
		logger.log('debug', 'Purger has finished');
	});
});

module.exports = app;