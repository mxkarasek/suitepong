const logger = require('winston');
var userDao = require("./../db/userDao.js");
var email = require("./../email/email.js");
var express = require('express');
var router = express.Router();

router.post('/', login);
router.post('/validateSession', validateSession);

function login(req, res, next) {
	logger.log('debug', "Login: " + JSON.stringify(req.body));

	userDao.addOrUpdate(req.body.userName, function (error, user) {
		if (error) {
			res.status(500).send(error);
		} else {
            email.sendLoginEmail(user.userName, function(error) {
                if (error) {
                    res.status(500).send(error);
                } else {
                    res.status(200).send(user);
                }
            });
		}
	})
}

function validateSession(req, res) {
	logger.log('debug', "Login: " + JSON.stringify(req.body));

	userDao.getBySessionId(req.body.sessionId, function (error, user) {
		if (error) {
			res.status(500).send(error);
		} else {
		    if (user) {
		        res.status(200).send(user)
		    } else {
		        userDao.getByCandidateSessionId(req.body.sessionId, function (error, user) {
                    if (error) {
                        logger.log('error', 'DB error: ' + JSON.stringify(error));
                        res.status(500).send(error);
                    } else {
                        if (user) {
                            user.sessionId = user.candidateSessionId;
                            user.candidateSessionId = "";
                            user.save(function (error) {
                                if (error) {
                                    logger.log('error', 'DB error: ' + JSON.stringify(error));
                                    res.status(500).send(error);
                                } else {
                                    if(user) {
                                        user.candidateSessionId = "";
                                        user.sessionId = "";
                                    }
                                    res.status(200).send(user)
                                }
                            });
                        } else {
                            res.status(403).send()
                        }
                    }
                });
		    }
		}
	})
}

module.exports = router;