var exports = module.exports = {};

const logger = require('winston');

exports.send = function (emailDto, handler) {
    logger.log('debug', "Sending email: " + JSON.stringify(emailDto));
    handler(null);
}