var exports = module.exports = {};

const logger = require('winston');
const sendGrid = require('sendgrid')(process.env.SENDGRID_API_KEY);
const mailBuilder = require('sendgrid').mail;

exports.send = function (email, handler) {
	logger.log('debug', "sending: " + JSON.stringify(email));

	var request = sendGrid.emptyRequest({
		method: 'POST',
		path: '/v3/mail/send',
		body: createEmail(email)
	});

	sendGrid.API(request, function (err, res) {
		if (err) logger.log('error', "SendGrid error: " + JSON.stringify(err, null, 2));
		handler(err, res);
	});
};

function createEmail(email) {
    const from = new mailBuilder.Email("SuitePong <suitepong@netsuite.com>");
    const to = new mailBuilder.Email(email.email);
    var content = new mailBuilder.Content("text/html", email.content);

    return (new mailBuilder.Mail(from, email.subject, to, content)).toJSON();
}