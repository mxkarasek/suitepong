var exports = module.exports = {};

const logger = require('winston');
var email = require("./service/" + process.env.EMAIL_SERVICE_PROVIDER + ".js");
var emailTemplate = require("./template.js");
var userDao = require("./../db/userDao.js");

exports.sendLoginEmail = function (userName, handler) {
    getCandidateSessionForUser(userName, function(error, candidateSessionId) {
        if(error) {
            handler(error);
        } else {
            emailContent = emailTemplate.renderHtmlEmail('login', {
               'loginUrl': "http://www.suitepong.com?sessionId=" + candidateSessionId
            });
            var emailDto = {
                'email': userName + "@netsuite.com",
                'subject': "SuitePong login confirmation",
                'content': emailContent
            }
            email.send(emailDto, handler);
        }
    });
}

exports.sendBookingConfirmationEmail = function (booking, handler) {
    emailContent = emailTemplate.renderHtmlEmail('bookingConfirmation', {
        'booking': booking
    })
    var emailDto = {
        'email': booking.userName + "@netsuite.com",
        'subject': "SuitePong booking confirmation",
        'content': emailContent
    }
    email.send(emailDto, handler);
}

function getCandidateSessionForUser(userName, handler) {
    userDao.getByUserName(userName, function(error, user) {
        if (error) {
            logger.log('debug', 'getByUserName('+ userName +'): ' + JSON.stringify(error));
        }
        handler(error, user.candidateSessionId)
    })
}