var exports = module.exports = {};

var dbConnection = require('./dbConnection.js');
var mongoose = dbConnection.getConnection();
var uuid = require('node-uuid');

const logger = require('winston');
logger.level = process.env.DEBUG_LEVEL;

var userSchema = new mongoose.Schema(
	{
		userName: String,
		sessionId: String,
		candidateSessionId: String
	},
	{
		collection: 'user'
	});

var User = mongoose.model('User', userSchema);
exports.User = User;

exports.getByUserName = function (userName, handler) {
    logger.log('debug', 'getByUserName : ' + JSON.stringify(userName));
    User
        .findOne({userName: userName}, {'_id': 0})
        .select('candidateSessionId')
        .exec(function (error, user) {
            if (error) {
                logger.log('error', 'DB Error: ', JSON.stringify(error));
            }
            handler(error, user)
        })
};

exports.getBySessionId = function (sessionId, handler) {
	logger.log('debug', 'getBySessionId : ' + JSON.stringify(sessionId));
	User
		.findOne({sessionId: sessionId}, {'_id': 0})
		.select('userName')
		.exec(function (error, user) {
			if (error) {
				logger.log('error', 'DB Error: ', JSON.stringify(error));
			}
			handler(error, user)
		})
};

exports.getByCandidateSessionId = function (sessionId, handler) {
	logger.log('debug', 'getByCandidateSessionId : ' + JSON.stringify(sessionId));
	User
		.findOne({candidateSessionId: sessionId})
		.select('userName sessionId candidateSessionId')
		.exec(function (error, user) {
			if (error) {
				logger.log('error', 'DB Error: ', JSON.stringify(error));
			}
			handler(error, user);
		})
};

exports.addOrUpdate = function (userName, handler) {
	logger.log('debug', 'addOrUpdate: ' + JSON.stringify(userName));
	var guid = uuid.v4();
	User
		.findOne({userName: userName})
		.select('userName sessionId candidateSessionId')
		.exec(function (error, user) {
			if (error) {
				logger.log('error', 'DB error: ' + JSON.stringify(error));
			}
			if (user) {
			    user.candidateSessionId = guid;
			    user.save(function (error) {
                    if (error) {
                        logger.log('error', 'DB error: ' + JSON.stringify(error));
                        handler(error, null);
                    } else {
                        if(user) {
                            user.candidateSessionId = "";
                            user.sessionId = "";
                        }
                        handler(null, user);
                    }
                });
			} else {
                var newUser = new User({
                    userName: userName,
                    candidateSessionId: guid
                });
                newUser.save(function (error) {
                    if (error) {
                        logger.log('error', 'DB error: ' + JSON.stringify(error));
                        handler(error, null);
                    } else {
                        if(newUser) {
                            newUser.candidateSessionId = "";
                            newUser.sessionId = "";
                        }
                        handler(null, newUser);
                    }
                });
			}
		});
};

exports.add = function (userName, sessionId, callback) {
    logger.log('debug', 'add user: ' + userName + ' / ' + sessionId);
    var newUser = new User({
        userName: userName,
        sessionId: sessionId,
        candidateSessionId: ''
    });
    newUser.save(function (error) {
        if (error) logger.log('error', error);
        callback();
    });
};