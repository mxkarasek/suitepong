var exports = module.exports = {};

var dbConnection = require('./dbConnection.js');
var mongoose = dbConnection.getConnection();
var autoIncrement = require('mongoose-auto-increment');

const logger = require('winston');
logger.level = process.env.DEBUG_LEVEL;

autoIncrement.initialize(mongoose.connection);

var bookingSchema = new mongoose.Schema(
	{
		id: Number,
		start: {type: Date},
		end: {type: Date},
		userName: String,
		confirmed: Boolean
	},
	{
		collection: 'booking'
	});

bookingSchema.plugin(autoIncrement.plugin, {model: 'Booking', field: 'id'});
var Booking = mongoose.model('Booking', bookingSchema);
exports.Booking = Booking;

exports.add = function (booking, callback) {
	logger.log('debug', 'add booking: ' + JSON.stringify(booking));
	var newBooking = new Booking({
		start: booking.start,
		end: booking.end,
		userName: booking.userName,
		confirmed: booking.confirmed
	});
	newBooking.save(function (error) {
		if (error) logger.log('error', error);
		callback(error, newBooking);
	});
};

exports.getAll = function (minStart, maxEnd, callback) {
	logger.log('debug', 'Get bookings starting: ' + minStart, "ending: " + maxEnd);
	Booking
		.find(
			{
				$and: [{start: {$gt: minStart}}
					, {end: {$lt: maxEnd}}]
			}
		)
		.select('id start end userName confirmed')
		.exec(function (error, bookings) {
			if (error) logger.log('error', error);
			callback(error, bookings)
		})
};

exports.getById = function (id, callback) {
	logger.log('debug', 'DB getById');
	if (id == undefined || id == "")
		return callback(new Error("missing ID"));

	Booking
		.findOne({id: id})
		.select('id start end userName confirmed')
		.exec(function (error, booking) {
			if (error) logger.log('error', error);
			callback(error, booking);
		});
};

exports.update = function(booking, callback) {
	logger.log('debug', 'Updating booking: ' + JSON.stringify(booking));
	var newBooking = {
		start: booking.start,
		end: booking.end,
		userName: booking.userName,
		confirmed: booking.confirmed
	};

	Booking
		.findOneAndUpdate({id: booking.id}, newBooking, {new: true})
		.exec(function (error, updatedBooking) {
			if (error) logger.log('error', error);
			callback(error, updatedBooking);
		});
};

exports.remove = function(booking, callback) {
	logger.log('debug', 'Deleting booking: ' + JSON.stringify(booking));
	Booking
		.findOne({id: booking.id})
		.remove()
		.exec(function(error) {
			if (error) logger.log('error', error);
			callback(error);
		});
};
