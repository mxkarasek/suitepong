const users = require('./../user/user.js');

const minute = 60 * 1000;
const hour = 60 * minute;
const day = 24 * hour;

const defaultConf = {
	maxBookingTime: 30 * minute,
	maxStartInFuture: 4 * hour,
	maxBookingsPerUser: 2,
	maxBookingsPerUserInterval: 12 * hour
};

var conf = {};
initConf();

var reasons = {
	bannedUser: function () {
		return 'User is banned';
	},
	tooLong: function () {
		return 'The booking is too long. Maximal bookable time is ' + conf.maxBookingTime/minute + ' minutes';
	},
	tooFarInFuture: function () {
		return 'The booking starts too far in future. You can book only in first ' + conf.maxStartInFuture/hour + ' hours';
	},
	conflictingBooking: function () {
		return 'The booking conflicts with other bookings';
	},
	numberOfBookingsExceeded: function () {
		return 'Number of bookings exceeded. You can only book ' + conf.maxBookingsPerUser + ' times in ' + conf.maxBookingsPerUserInterval/hour + 'hours';
	}
};

function apply(booking, existingBookings, user, callback) {
	if (isBannedUser(user))
		return callback(null, {accept: false, reason: reasons.bannedUser()});

	if (isTooFarInFuture(booking))
		return callback(null, {accept: false, reason: reasons.tooFarInFuture()});

	if (isTooLongBooking(booking))
		return callback(null, {accept: false, reason: reasons.tooLong()});

	if (isConflictingBooking(booking, existingBookings))
		return callback(null, {accept: false, reason: reasons.conflictingBooking()});

	if (isNumberOfBookingsPerIntervalExceeded(booking, existingBookings, user))
		return callback(null, {accept: false, reason: reasons.numberOfBookingsExceeded()});

	return callback(null, {accept: true});
}

function isBannedUser(user) {
	return user.banned;
}

function isTooLongBooking(booking) {
	return (booking.end - booking.start) > conf.maxBookingTime;
}

function isTooFarInFuture(booking) {
	return booking.start > (new Date().getTime() + conf.maxStartInFuture);
}

function isConflictingBooking(newBooking, existingBookings) {
	return existingBookings
			.filter(function (booking) {
				return (
				(booking.start <= newBooking.start && booking.end > newBooking.start) ||
				(booking.start < newBooking.end && booking.end >= newBooking.end) ||
				(booking.start >= newBooking.start && booking.end <= newBooking.end));
			}).length > 0;
}

function isNumberOfBookingsPerIntervalExceeded(booking, existingBookings, user) {
	if (user == users.anonymousUser)
		return false;

	return existingBookings
		.filter(function (booking) {
			return booking.userName == user.userName;
		})
		.concat([booking])
		.reduce(function (prev, cur, idx, bookings) {
			const window = {
				start: new Date(cur.start).getTime(),
				end: new Date(cur.start).getTime() + conf.maxBookingsPerUserInterval
			};
			return prev || getBookingsInWindow(window, bookings).length > conf.maxBookingsPerUser;
		}, false);

	function getBookingsInWindow(window, bookings) {
		return bookings
			.filter(function (booking) {
				return (booking.start > window.start && booking.start < window.end) ||
					(booking.end > window.start && booking.end < window.end) ||
					(booking.start <= window.start && booking.end >= window.end);
			});
	}
}

function initConf() {
	conf.maxBookingTime = defaultConf.maxBookingTime;
	conf.maxStartInFuture = defaultConf.maxStartInFuture;
	conf.maxBookingsPerUser = defaultConf.maxBookingsPerUser;
	conf.maxBookingsPerUserInterval = defaultConf.maxBookingsPerUserInterval;
}

module.exports = {
	conf: conf,
	resetConf: initConf,
	apply: apply,
	reasons: reasons
};