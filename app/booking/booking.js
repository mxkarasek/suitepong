const logger = require('winston');
const bookingDao = require("./../db/bookingDao.js");
const Users = require('./../user/user');
const userDao = require("./../db/userDao.js");
const email = require("./../email/email.js");
const express = require('express');
const Rules = require('./bookingRules.js');
const router = express.Router();

const minute = 60 * 1000;
const hour = 60 * minute;
const day = 24 * hour;
const minMatchDuration = 10 * minute;

router.post('/', addNewBooking);
router.get('/', getBookings);
router.post('/:id/confirm', confirmBooking);
router.get('/:id', getBooking);
router.post('/now', addInstantBooking);
router.get('/query/current', getCurrentBooking);
router.get('/query/nextfree', getNextFreeSlot);

function addNewBooking(req, res) {
	logger.log('debug', "Add new booking:" + JSON.stringify(req.body, null, 2));

	addBooking(req.body, function (error, booking) {
		if (error) {
			res.status(500).send(error.message);
		} else {
			email.sendBookingConfirmationEmail(booking, function (error) {
				if (error) {
					logger.log('error', error);
					res.status(500).send("Failed to send confirmation for new booking");
				} else {
					res.status(200).send(booking);
				}
			});
		}
	});
}

function confirmBooking(req, res) {
	logger.log('debug', "Confirm booking id: " + req.params.id);
	withExistingBooking(req.params.id, function (error, booking) {
		if (error) return res.status(500).send(error.message);

		withConfirmableBooking(booking, function (error, booking) {
			if (error) return res.status(500).send(error.message);

			confirmBookingInDb(booking, function (error, booking) {
				if (error) {
					logger.log('error', error);
					res.status(500).send("Error while loading booking");
				} else {
					res.status(200).send(booking);
				}
			});
		});
	});
}

function addInstantBooking(req, res) {
	logger.log('debug', "Add new booking now:" + JSON.stringify(req.body, null, 2));

	const newBooking = {
		start: new Date(),
		end: new Date(new Date().getTime() + (parseInt(req.body.duration) * minute))
	};

	addBookingForUser(newBooking, Users.anonymousUser, function (error, booking) {
		if (error) {
			res.status(500).send(error.message);
		} else {
			res.status(200).send(booking);
		}
	});
}

function getBookings(req, res) {
	logger.log('debug', "Get all bookings");
	getAll(function (error, bookings) {
		if (error) {
			logger.log('error', error);
			res.status(500).send("Error while loading all bookings");
		} else {
			res.status(200).send(bookings);
		}
	});
}

function getBooking(req, res) {
	logger.log('debug', 'Get booking for id: ' + req.params.id);
	bookingDao.getById(req.params.id, function (error, booking) {
		if (error) {
			logger.log('error', error);
			res.status(500).send("Error while loading booking");
		} else {
			res.status(200).send(booking);
		}
	});
}

function getCurrentBooking(req, res) {
	logger.log('debug', 'Get current booking');
	getAll(function (error, bookings) {
		if (error) {
			logger.log('error', error);
			res.status(500).send("Error while loading bookings");
		} else {
			res.status(200).send(getFirstFutureBooking(bookings, new Date()));
		}
	});
}

function getNextFreeSlot(req, res) {
	logger.log('debug', 'Find next free slot');
	getAll(function (error, bookings) {
		if (error) {
			logger.log('error', error);
			res.status(500).send("Error while loading bookings");
		} else {
			res.status(200).send(getFirstFreeSlotDate(bookings, new Date()));
		}
	});
}

function getFirstFutureBooking(bookings, now) {
	return bookings
		.filter(function (booking) {
			return booking.end > now;
		})
		.sort(function (a, b) {
			return a.end < b.end;
		})
		.pop();
}

function getFirstFreeSlotDate(bookings, startDate) {
	logger.log('debug', 'get first free slot for date: ' + startDate);
	var firstFutureBooking = getFirstFutureBooking(bookings, startDate);
	if (!firstFutureBooking || (firstFutureBooking.start.getTime() - startDate) > minMatchDuration) {
		return startDate;
	} else {
		return getFirstFreeSlotDate(bookings, firstFutureBooking.end);
	}
}

function getAll(callback) {
	var now = new Date();
	var maxEnd = new Date(now.getTime() + day);
	var minStart = new Date(now.getTime() - day);
	logger.log('debug', "Find all bookings from: " + minStart + "to: " + maxEnd);

	bookingDao.getAll(minStart, maxEnd, callback);
}

function withValidSession(sessionId, callback) {
	if (!sessionId) return callback(new Error("Missing session ID"));

	userDao.getBySessionId(sessionId, function (error, user) {
		if (error) return callback(error);
		if (!user) return callback(new Error("Invalid session ID: " + sessionId));

		return callback(null, user);
	});
}

function withExistingBooking(bookingId, callback) {
	bookingDao.getById(bookingId, function (error, booking) {
		callback(error, booking);
	});
}

function withConfirmableBooking(booking, callback) {
	const timeAfterStart = new Date().getTime() - new Date(booking.start).getTime();
	if (timeAfterStart < 0)
		return callback(new Error("Cannot confirm future booking"));

	callback(null, booking);
}

function confirmBookingInDb(booking, callback) {
	var newBooking = booking;
	newBooking.confirmed = true;
	bookingDao.update(newBooking, function (error, updatedBooking) {
		callback(error, updatedBooking);
	});
}

/**
 * The function returns user facing error
 */
function addBooking(bookingRequest, callback) {
	withValidSession(bookingRequest.sessionId, function (error, user) {
		if (error) {
			logger.log('error', error);
			return callback(new Error('Unknown user'));
		}

		addBookingForUser(bookingRequest, user, callback);
	});
}

/**
 * The function returns user facing error
 */
function addBookingForUser(bookingRequest, user, callback) {
	withValidBooking(bookingRequest, user, function (error) {
		if (error) return callback(error);

		const booking = {
			start: bookingRequest.start,
			end: bookingRequest.end,
			userName: user.userName,
			confirmed: user == Users.anonymousUser
		};

		bookingDao.add(booking, function (error, booking) {
			if (error) {
				logger.log('error', error);
				return callback(new Error('Failed to add new booking'), booking);
			}
			callback(error, booking);
		});
	});
}

function validateInputBookingData(booking) {
	logger.log('debug', 'Validating request' + JSON.stringify(booking, null, 2));
	var errors = [];

	if (booking.start == undefined || isNaN(new Date(booking.start).getTime()))
		errors.push("missing or invalid start date/time");

	if (booking.end == undefined ||
		isNaN(new Date(booking.end).getTime()) ||
		(new Date(booking.end).getTime() < new Date().getTime()))
		errors.push("missing or invalid end date/time");

	if (new Date(booking.end) < new Date(booking.start))
		errors.push("date/time to has to be after date/time from");

	if (errors.length > 0)
		return new Error(errors);
}

/**
 * The function returns user facing error
 */
function withValidBooking(booking, user, callback) {
	var inputDataValidationResult = validateInputBookingData(booking);
	if (inputDataValidationResult instanceof Error)
		return callback(inputDataValidationResult);

	getAll(function (error, bookings) {
		if (error) {
			logger.log('error', error);
			return callback(new Error('Failed to validate new booking'));
		}

		var newBooking = {
			start: new Date(booking.start),
			end: new Date(booking.end),
			userName: booking.userName
		};

		Rules.apply(newBooking, bookings, user, function (err, res) {
			if (err) {
				logger.log('error', err);
				return callback(new Error('Failed to process rules for the booking'));
			}
			return callback(res.accept ? null : new Error('Invalid booking, reason: ' + res.reason));
		})
	});
}

module.exports = router;