const logger = require('winston');
logger.level = process.env.DEBUG_LEVEL;
const bookingDao = require('./../db/bookingDao.js');

const minute = 60 * 1000;
const hour = 60 * minute;

var conf = {
	confirmationLimit: 5 * minute
};

function purge(callback) {
	logger.log('debug', 'Purging expired bookings');

	withExpiredBookings(function(err, expiredBookings) {
		if (err) return callback(new Error('Failed to get expired bookings'));

		var purgingError;
		var waitForResult = expiredBookings.length;
		expiredBookings.map(function (booking) {
			bookingDao.remove(booking, function (err) {
				if (err) {
					logger.log('Failed to remove booking: ' + JSON.stringify(booking) + 'error: ' + error);
					purgingError = new Error("Failed to remove some of the bookings");
				}
				if (!(--waitForResult)) return callback(purgingError);
			})
		});

		if (!waitForResult) return callback();
	});
}

function withExpiredBookings(callback) {
	const now = new Date().getTime();
	const searchStart = new Date(now - 12 * hour);
	const searchEnd = new Date(now + 2 * hour);

	bookingDao.getAll(searchStart, searchEnd, function (err, bookings) {
		if (err) {
			logger.log('error', err);
			return callback(new Error('Failed to get all bookings'));
		}
		var expiredBookings = bookings.filter(function (booking) {
			return !booking.confirmed && booking.start < (now - conf.confirmationLimit);
		});

		callback(null, expiredBookings);
	});
}


module.exports = {
	purge: purge,
	conf: conf
};