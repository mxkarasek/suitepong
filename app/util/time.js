const logger = require('winston');
var express = require('express');
var router = express.Router();

router.get('/', getTime);

function getTime(req, res) {
	logger.log('debug', "Get time");
	res.status(200).send(new Date());
}

module.exports = router;