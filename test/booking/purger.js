process.env.NODE_ENV = 'test';
const chai = require('chai');
const purger = require('../../app/booking/purger.js');
const bookingDao = require('../../app/db/bookingDao.js');

const minute = 60 * 1000;
const hour = 60 * minute;

describe('purger', function () {

	it('should clean unconfirmed expired bookings', function (done) {
		withEmptyDb(function (err) {
			if (err) return done(err);

			withNewBooking(expiredBooking, function (err, booking) {
				if (err) return done(err);

				purger.purge(function (err) {
					if (err) return done(err);

					findBooking(booking.id, function (err, booking) {
						if (err) return done(err);

						chai.assert(!booking);
						done();
					});
				});
			})
		});
	});

	it('should not clean confirmed booking', function (done) {
		withEmptyDb(function (err) {
			if (err) return done(err);

			withNewBooking(confirmedBooking, function (err, booking) {
				if (err) return done(err);

				purger.purge(function (err) {
					if (err) return done(err);

					findBooking(booking.id, function (err, storedBooking) {
						if (err) return done(err);

						chai.expect(storedBooking.id).equal(booking.id);
						done();
					});
				});
			})
		});
	});

	it('should not clean unconfirmed future booking', function (done) {
		withEmptyDb(function (err) {
			if (err) return done(err);

			withNewBooking(futureBooking, function (err, booking) {
				if (err) return done(err);

				purger.purge(function (err) {
					if (err) return done(err);

					findBooking(booking.id, function (err, storedBooking) {
						if (err) return done(err);

						chai.expect(storedBooking.id).equal(booking.id);
						done();
					});
				});
			})
		});
	});

	it('should clean only expired booking', function (done) {
		withEmptyDb(function (err) {
			if (err) return done(err);

			withNewBooking(expiredBooking, function (err, newExpiredBooking) {
				if (err) return done(err);

				withNewBooking(confirmedBooking, function (err, newConfirmedBooking) {
					if (err) return done(err);

					withNewBooking(futureBooking, function (err, newFutureBooking) {
						if (err) return done(err);

						purger.purge(function (err) {
							if (err) return done(err);

							findBooking(newExpiredBooking.id, function (err, storedExpiredBooking) {
								if (err) return done(err);

								chai.assert(!storedExpiredBooking);
								findBooking(newConfirmedBooking.id, function (err, storedConfirmedBooking) {
									if (err) return done(err);

									chai.expect(storedConfirmedBooking.id).equal(newConfirmedBooking.id);
									findBooking(newFutureBooking.id, function (err, storedFutureBooking) {
										if (err) return done(err);

										chai.expect(storedFutureBooking.id).equal(newFutureBooking.id);
										done();
									});
								});
							});
						});
					});
				});
			});
		});
	});

	it('should not purge non confirmed booking that have not pass the set threshold', function(done) {
		withEmptyDb(function (err) {
			if (err) return done(err);

			purger.conf.confirmationLimit = hour;
			withNewBooking(unconfirmedBooking, function (err, booking) {
				if (err) return done(err);

				purger.purge(function (err) {
					if (err) return done(err);

					findBooking(booking.id, function (err, storedBooking) {
						if (err) return done(err);

						chai.expect(storedBooking.id).equal(booking.id);
						done();
					});
				});
			})
		});
	});

});

function withEmptyDb(callback) {
	bookingDao.Booking
		.find({})
		.remove()
		.exec(callback);
}

const expiredBooking = {
	start: new Date().getTime() - 3 * hour,
	end: new Date().getTime() - 2 * hour,
	userName: 'test',
	confirmed: false
};

const confirmedBooking = {
	start: new Date().getTime() - hour,
	end: new Date().getTime(),
	userName: 'test',
	confirmed: true
};

const futureBooking = {
	start: new Date().getTime() + hour,
	end: new Date().getTime() + 2 * hour,
	userName: 'test',
	confirmed: false
};

const unconfirmedBooking = {
	start: new Date().getTime() - 30 * minute,
	end: new Date().getTime() + hour,
	userName: 'test',
	confirmed: false
};


function withNewBooking(booking, callback) {
	new bookingDao.Booking(booking).save(callback);
}

function findBooking(id, callback) {
	bookingDao.getById(id, callback);
}



