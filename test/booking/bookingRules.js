process.env.NODE_ENV = 'test';

const chai = require('chai');
chai.use(require('chai-http'));
const Rules = require('../../app/booking/bookingRules.js');
const Users = require('../../app/user/user.js');

const minute = 60 * 1000;
const hour = 60 * minute;

describe('booking rules', function () {

	beforeEach(function () {
		Rules.resetConf();
	});

	it('should not be possible to create a booking for a banned user', function (done) {
		Rules.apply(newBooking, [], bannedUser, function (err, res) {
			if (err) return done(err);
			chai.expect(res.reason).contains(Rules.reasons.bannedUser());
			chai.assert(!res.accept);
			done();
		});
	});

	it('should not be possible to book for more than defined duration', function (done) {
		Rules.conf.maxBookingTime = 30 * minute;
		Rules.apply(newBooking, [], user, function (err, res) {
			if (err) return done(err);
			chai.expect(res.reason).contains(Rules.reasons.tooLong());
			chai.assert(!res.accept);
			done();
		})
	});

	it('should not be possible to book too far in the future', function (done) {
		Rules.conf.maxStartInFuture = 30 * minute;
		Rules.conf.maxBookingTime = hour;
		Rules.apply(newBooking, [], user, function (err, res) {
			if (err) return done(err);
			chai.expect(res.reason).contains(Rules.reasons.tooFarInFuture());
			chai.assert(!res.accept);
			done();
		})
	});

	it('should not be possible to book a conflicting booking', function (done) {
		Rules.conf.maxBookingTime = hour;
		Rules.apply(conflictingBooking, inRowBookings, testUser, function (err, res) {
			if (err) return done(err);
			chai.expect(res.reason).contains(Rules.reasons.conflictingBooking());
			chai.assert(!res.accept);
			done();
		})
	});

	it('should not be possible to book if the user exceeded the number of bookings per interval', function (done) {
		Rules.conf.maxBookingTime = hour;
		Rules.conf.maxBookingsPerUser = 2;
		Rules.conf.maxBookingsPerUserInterval = 3 * hour;
		Rules.apply(newBooking, spacedBookings, testUser, function (err, res) {
			if (err) return done(err);
			chai.expect(res.reason).contains(Rules.reasons.numberOfBookingsExceeded());
			chai.assert(!res.accept);
			done();
		});
	});

	it('should be possible to book for the anonymous user even if he exceeded the number of bookings per interval', function (done) {
		Rules.conf.maxBookingTime = hour;
		Rules.conf.maxBookingsPerUser = 2;
		Rules.conf.maxBookingsPerUserInterval = 3 * hour;
		Rules.apply(newBooking, spacedBookings, Users.anonymousUser, function (err, res) {
			if (err) return done(err);
			chai.assert(res.accept);
			done();
		});
	});


	it('should be possible to book if the user did not exceed the number of bookings per interval', function (done) {
		Rules.conf.maxBookingTime = hour;
		Rules.conf.maxBookingsPerUser = 3;
		Rules.conf.maxBookingsPerUserInterval = 4 * hour;
		Rules.apply(newBooking, spacedBookings, testUser, function (err, res) {
			if (err) return done(err);
			chai.assert(res.accept);
			done();
		});
	});
});


const bannedUser = {
	userName: 'test',
	banned: true
};

const testUser = {
	userName: 'test'
};

const user = {
	userName: 'user'
};

const now = new Date().getTime();

const conflictingBooking = {
	start: now,
	end: now + hour,
	userName: user.userName
};

const inRowBookings = [
	{
		start: now,
		end: now + hour,
		userName: testUser.userName
	},
	{
		start: now + hour,
		end: now + 2 * hour,
		userName: testUser.userName
	},
	{
		start: now + 2 * hour,
		end: now + 3 * hour,
		userName: testUser.userName
	}
];

const spacedBookings = [
	{
		start: now,
		end: now + hour,
		userName: testUser.userName
	},
	{
		start: now + 2 * hour,
		end: now + 3 * hour,
		userName: testUser.userName
	},
	{
		start: now + 4 * hour,
		end: now + 5 * hour,
		userName: testUser.userName
	}
];


const newBooking = {
	start: now + hour,
	end: now + 2 * hour,
	userName: testUser.userName
};
