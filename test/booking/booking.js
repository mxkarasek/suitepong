process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaihttp = require('chai-http');
var should = chai.should();
chai.use(chaihttp);
const logger = require('winston');
logger.level = process.env.DEBUG_LEVEL;

var bookingDAO = require('../../app/db/bookingDao.js');
var userDAO = require('../../app/db/userDao.js');
var app = require('../../server/server.js');

const validSessionId = 'abse54215s';

const minute = 60 * 1000;
const hour = 60 * minute;

describe("SuitePong API", function () {
	before(function (done) {
		withEmptyDb(function () {
			userDAO.add('Test', validSessionId, done, false);
		});
	});

	describe('post /v1/booking/now', function () {
		it('should create new booking if free slot is available', function (done) {
			chai.request(app)
				.post('/v1/booking/now')
				.send({duration: 10})
				.end(function (err, res) {
					chai.expect(res.status).equal(200);
					verifyInstantBookingObject(res);
					done();
				});
		})
	});

	describe('get /v1/booking/query/nextfree', function () {
		it('should return now when there are no bookings', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);
				chai.request(app)
					.get('/v1/booking/query/nextfree')
					.end(function (err, res) {
						chai.expect(res.status).equal(200);
						chai.assert(Math.abs(new Date(res.body).getTime() - new Date().getTime()) < 10 * 1000,
							"unexpected start time: " + new Date(res.body));
						done();
					})
			});
		})
	});

	describe('post /v1/booking/:id/confirm', function () {
		it('should confirm the booking', function (done) {
			chai.request(app)
				.post('/v1/booking')
				.send(getTestBooking(new Date(new Date().getTime()), 15))
				.end(function (err, res) {
					if (err) return done(err);
					chai.request(app)
						.post('/v1/booking/' + res.body.id + '/confirm')
						.send('{}')
						.end(function (err, res) {
							if (err) return done(err);
							chai.expect(res.body.confirmed).equal(true);
							done();
						});
				});
		});

		it('should not be possible to confirm future booking', function (done) {
			chai.request(app)
				.post('/v1/booking')
				.send(getTestBooking(new Date(new Date().getTime() + hour), 15))
				.end(function (err, res) {
					if (err) return done(err);
					chai.request(app)
						.post('/v1/booking/' + res.body.id + '/confirm')
						.send('{}')
						.end(function (err, res) {
							chai.expect(res.status).not.equal(200);
							done();
						});
				});
		});
	});

	describe('post /v1/booking', function () {
		it('should not allow to create a booking if sessionId is missing', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);

				var booking = getTestBooking(new Date(), 20);
				booking.sessionId = '';

				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			})
		});

		it('should not allow to create a booking that ends in past', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);

				const booking = getTestBooking(new Date(new Date().getTime() - 2 * hour), 15);
				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			})
		});

		it('should not allow to create a booking if sessionId is not valid', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);

				var booking = getTestBooking(new Date(), 20);
				booking.sessionId = 'invalid-session-id';

				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			})
		});

		it('should not allow to create a booking longer than 30 minutes', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);

				var booking = getTestBooking(new Date(), 31);

				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			})
		});

		it('should not allow to create a booking for more than 4 hours in future', function (done) {
			withEmptyDb(function (error) {
				if (error) return done(error);

				var booking = getTestBooking(new Date(new Date().getTime() + 5 * hour), 20);

				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			})
		});
	});

	describe('Create and Get booking', function () {
		var createdRecordId;
		var booking = getTestBooking(new Date(), 15);

		describe("Create new booking", function () {
			it("should create new booking", function (done) {
				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						res.should.have.status(200);
						verifyBookingObject(res);
						createdRecordId = res.body.id;
						done();
					});
			});
		});

		describe("Get booking by ID", function () {
			it("should return booking by ID", function (done) {
				chai.request(app)
					.get('/v1/booking/' + createdRecordId)
					.end(function (err, res) {
						res.should.have.status(200);
						verifyBookingObject(res);
						done();
					});
			});

		});
	});

	describe("Get booking list", function () {
		it("should return array of bookings", function (done) {
			chai.request(app)
				.get('/v1/booking')
				.end(function (err, res) {
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('array');
					done();
				});
		});
	});

	describe('Create conflict booking', function () {
		it('should return error for the conflicting booking', function (done) {
			const startDate = new Date(new Date().getTime());
			var firstBooking = getTestBooking(startDate, 15);
			var secondBooking = getTestBooking(new Date(startDate.getTime() + minutesToMillis(10)), 15);

			withEmptyDb(function (error) {
				if (error) done(error);
				chai.request(app)
					.post('/v1/booking')
					.send(firstBooking)
					.end(function (err, res) {
						chai.expect(res.status).equal(200);
						chai.request(app)
							.post('/v1/booking')
							.send(secondBooking)
							.end(function (err, res) {
								chai.expect(res.status).not.equal(200);
								done()
							});
					});
			});
		});
	});

	describe('Get current or next booking', function () {
		it('should return booking object', function (done) {
			var booking = getTestBooking(new Date(), 15);

			withEmptyDb(function (error) {
				if (error) return done(error);
				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).equal(200);
						chai.request(app)
							.get('/v1/booking/query/current')
							.end(function (err, res) {
								res.should.have.status(200);
								verifyBookingObject(res);
								done();
							});
					});
			});
		});
	});

	describe('Get next free time', function () {
		it('should return next free time', function (done) {
			chai.request(app)
				.get('/v1/booking/query/nextfree')
				.end(function (err, res) {
					res.should.have.status(200);
					done();
				});
		});
	});

	describe('Create more booking per day than is allowed', function () {
		it('should return error for the third booking', function (done) {
			const startDate = new Date(new Date().getTime());
			var firstBooking = getTestBooking(startDate, 15);
			var secondBooking = getTestBooking(new Date(startDate.getTime() + minutesToMillis(60)), 15);
			var thirdBooking = getTestBooking(new Date(startDate.getTime() + minutesToMillis(120)), 15);

			withEmptyDb(function (error) {
				if (error) done(error);
				chai.request(app)
					.post('/v1/booking')
					.send(firstBooking)
					.end(function (err, res) {
						chai.expect(res.status).equal(200);
						chai.request(app)
							.post('/v1/booking')
							.send(secondBooking)
							.end(function (err, res) {
								chai.expect(res.status).equal(200);
								chai.request(app)
									.post('/v1/booking')
									.send(thirdBooking)
									.end(function (err, res) {
										chai.expect(res.status).not.equal(200);
										done()
									});
							});
					});
			});
		});
	});

	describe('Create too long booking', function () {
		it('should return error for this booking', function (done) {
			var booking = getTestBooking(new Date(new Date().getTime()), 31);

			withEmptyDb(function (error) {
				if (error) done(error);
				chai.request(app)
					.post('/v1/booking')
					.send(booking)
					.end(function (err, res) {
						chai.expect(res.status).not.equal(200);
						done();
					});
			});
		});
	});
});

function verifyBookingheader(res)
{
	res.should.be.json;
	res.body.should.be.a('object');
	res.body.should.have.property('id');
	res.body.should.have.property('userName');
	res.body.should.have.property('start');
	res.body.should.have.property('end');
}

function verifyBookingObject(res) {
	verifyBookingheader(res);
	res.body.confirmed.should.equal(false);
}

function verifyInstantBookingObject(res) {
	verifyBookingheader(res);
	res.body.confirmed.should.equal(true);
}

function withEmptyDb(callback) {
	bookingDAO.Booking
		.find({})
		.remove()
		.exec(callback);
}

function getTestBooking(startDate, durationInMinutes) {
	return {
		start: startDate,
		end: new Date(startDate.getTime() + minutesToMillis(durationInMinutes)),
		sessionId: validSessionId
	};
}

function minutesToMillis(minutes) {
	return minutes * 60 * 1000;
}