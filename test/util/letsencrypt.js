process.env.NODE_ENV = 'test';

const chai = require('chai');
chai.use(require('chai-http'));

const app = require('../../server/server.js');

describe('get /.well-known/acme-challenge/:id', function() {
	it("should return a token", function (done) {
		chai.request(app)
			.get('/.well-known/acme-challenge/token')
			.end(function (err, res) {
				chai.expect(res.status).equal(200);
				done();
			})
	});
});