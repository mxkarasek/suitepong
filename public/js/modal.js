function getCurrentTimeAsString() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    return zeroPad(hours) + ':' + zeroPad(minutes);
}

function computeQuickGameEnd(start) {
    var endTime = new Date();
    endTime.setMinutes(endTime.getMinutes() + 15);
    return zeroPad(endTime.getHours() + ":" + zeroPad(endTime.getMinutes()));
}

function submitNewBooking() {
    console.log('Submitting new booking.' + createBookingJson());
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/v1/booking/",
        data: createBookingJson(),
        success: function (data) {
            location.reload();
        }
    })
}

function createBookingJson() {
    var start = $('#start').timepicker('getTime', new Date());
    var end = $('#end').timepicker('getTime');
    var username = $('#username').val();

    return {"start": start.toISOString(), "end": end.toISOString(), "userName": username};
}

function zeroPad(number) {
    if (number < 10)
        return "0" + number;
    else
        return "" + number;
}