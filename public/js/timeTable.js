function TimeTable(config) {

	config.fromHour = config.fromHour ? config.fromHour : 8;
	config.toHour = config.toHour ? config.toHour : 19;

	var $timeTable = $(config.container);
	var maxBookingTime = config.maxBookingTime ? config.maxBookingTime : 30; // minutes

	var headerUpdater = new HeaderUpdater();

	this.init = function() {
		initEmptyTimeTable();
		initSelectable();
		pongUtil.callAndSetInterval(refreshPanels, 30);
	};

	function populate() {
		$.get("/v1/booking", processBookings);
	}

	function initEmptyTimeTable() {
		for (var hour = config.fromHour; hour < config.toHour; hour++) {
			var $row = $("<tr>", {"data-hour": hour});
			$timeTable.append($row);
			var $header = $("<th>", {"class": "hour"});
			$header.text(dateUtil.padWithZero(hour));
			$row.append($header);

			for (var minutes = 0; minutes < 60; minutes = minutes + 5) {
				$row.append($("<td>", {"class": "col-md-1 booking-cell", "data-minutes": minutes}));
			}
		}
	}

	function processBookings(bookings) {
		var now = new Date();
		bookings
				.filter(function(booking) {
					var start = new Date(booking.start);
					return dateUtil.compareByDate(now, start) == 0;
				})
				.map(function(booking) {
					showBookingInTimeTable(booking);
				});

		highlightBookingsOfCurrentUser();
	}

	function showBookingInTimeTable(booking) {
		var from = dateUtil.roundDownToFiveMinutes(new Date(booking.start));
		var to = dateUtil.roundUpToFiveMinutes(new Date(booking.end));
		var name = booking.userName;

		var colspan = (to - from) / 300000;
		var colspanFirstRow = colspan;
		var colspanSecondRow = 0;
		if ((from.getMinutes() / 5) + colspan > 12) {
			colspanFirstRow = 12 - (from.getMinutes() / 5);
			colspanSecondRow = colspan - colspanFirstRow;
		}

		var $row = $timeTable.find("[data-hour='" + from.getHours() + "']");
		var $pivot = $row.find("[data-minutes='" + from.getMinutes() + "']");
		$pivot.attr("colspan", colspanFirstRow);
		$pivot.addClass("booked");
		$pivot.attr("data-user", name);

		hideNextNumberOfSiblings($pivot, colspanFirstRow - 1);

		if (colspanSecondRow == 0) {
			$pivot.addClass("booked-start booked-end");
			$pivot.text(dateUtil.formatTimeInterval(from, to) + " " + name);
		} else {
			$pivot.addClass("booked-start");

			$row = $row.next();
			var $secondRowPivot = $row.find("[data-minutes='0']");
			$secondRowPivot.attr("colspan", colspanSecondRow);
			$secondRowPivot.addClass("booked-end");
			$secondRowPivot.addClass("booked");
			$secondRowPivot.attr("data-user", name);
			hideNextNumberOfSiblings($secondRowPivot, colspanSecondRow - 1);

			var ratio = colspanFirstRow / colspanSecondRow;
			if (ratio >= 2) {
				$pivot.text(dateUtil.formatTimeInterval(from, to) + " " + name);
			} else if (ratio <= 0.5) {
				$secondRowPivot.text(dateUtil.formatTimeInterval(from, to) + " " + name);
			} else {
				$pivot.text(dateUtil.formatTimeInterval(from, to));
				$secondRowPivot.text(name);
			}
		}
	}

	function hideNextNumberOfSiblings(element, number) {
		var current = element.next();
		for (var j = 0; j < number; j++) {
			current.addClass("hidden");
			current = current.next();
		}
	}

	var $firstSelected = null;
	var $endSelected = null;

	function initSelectable() {
		$timeTable.find(".booking-cell").on("click", function() {
			if ($(this).hasClass("booked") || $(this).hasClass("unavailable")) {
				return;
			}
			getDateFromCell($(this));
			if ($firstSelected == null || ($firstSelected && $endSelected)) {
				clearSelected();
				$firstSelected = $(this);
				$endSelected = null;
				$(this).addClass("selected booked-start");
			} else if ($firstSelected) {
				if (compareCells($(this), $firstSelected) == 0) {
					clearSelectedHover();
					$firstSelected.removeClass("selected booked-start");
					$firstSelected = null;
				} else if (!$(this).hasClass("selected-hover")) {
					clearSelectedHover();
					$firstSelected.removeClass("selected booked-start");
					$firstSelected = $(this);
					$firstSelected.addClass("selected booked-start");
				} else {
					$endSelected = $(this);
					$timeTable.find(".booking-cell.selected-hover").each(function() {
						$(this).removeClass("selected-hover");
						$(this).addClass("selected");
					});
					$endSelected.removeClass("selected-hover");
					$endSelected.addClass("selected booked-end");

					if (!new Auth().getSessionId())
						pongUtil.showMessage('You need to log in to create a reservation.', 'warning');
					else
						showConfirmationDialog($firstSelected, $endSelected);
				}
			}
		});

		$timeTable.find(".booking-cell").on("mouseover", function() {
			if ($firstSelected && !$endSelected) {
				clearSelectedHover();
				if (compareCells($(this), $firstSelected) <= 0) {
					return;
				}
				var $current = $firstSelected;
				var numberOfCurrentlySelected = 1;
				while (true) {
					var $next = getNextCell($current);
					numberOfCurrentlySelected++;
					if ($next.hasClass("booked") || numberOfCurrentlySelected > maxBookingTime / 5) {
						break;
					}
					$current = $next;
					if (compareCells($(this), $next) == 0) {
						break;
					}
					$current.addClass("selected-hover");
				}
				$current.addClass("selected-hover booked-end");
			}
		});
	}

	function getDateFromCell(cell) {
		var minutes = cell.data("minutes");
		var hour = cell.parent().data("hour");
		return dateUtil.getTodayByTime(hour, minutes);
	}

	function submitBooking(start, end, sessionId) {
		$.post("/v1/booking", {start: start, end: end, sessionId: sessionId})
				.done(function(data) {
					console.log("new booking success:\n" + JSON.stringify(data));
					refreshPanels();
				})
				.fail(function(data) {
					console.log("new booking error:\n" + JSON.stringify(data));
					pongUtil.showMessage(data.responseText, 'error', function() {
						location.reload();
					});
				});
	}

	function refreshPanels() {
		populate();
		disableBookingOfCells();
		headerUpdater.update();
	}

	function clearSelectedHover() {
		$timeTable.find(".booking-cell.selected-hover").removeClass("selected-hover booked-end");
	}

	function clearSelected() {
		$timeTable.find(".booking-cell.selected").removeClass("selected booked-start booked-end");
	}

	function getNextCell($current) {
		var $next = $current.next();
		if ($next.length == 0) {
			$next = $current.parent().next().children(".booking-cell").first();
		}
		return $next;
	}

	/** Returns negative number, if first is before second. Positive number if first is after second. Zero if they are the same. */
	function compareCells($first, $second) {
		var result = $first.parent().index() - $second.parent().index();
		if (result == 0) {
			result = $first.index() - $second.index();
		}
		return result;
	}

	function disableBookingOfCells() {
		var now = dateUtil.roundUpToFiveMinutes(new Date());
		$timeTable.find(".booking-cell").each(function() {
			var date = dateUtil.getTodayByTime($(this).parent().data("hour"), $(this).data("minutes"));
			if (date < now) {
				$(this).addClass("unavailable");
			}
		});
	}

	function highlightBookingsOfCurrentUser() {
		var currentUser = $("#logged-in-user").text();
		$timeTable.find(".booking-cell").each(function() {
			if (currentUser == $(this).data("user")) {
				$(this).addClass("highlight");
			} else {
				$(this).removeClass("highlight");
			}
		});
	}

	function showConfirmationDialog(startCell, endCell) {
		var start = getDateFromCell(startCell);
		var end = dateUtil.addMinutes(getDateFromCell(endCell), 5);
		newBookingConfirmationDialogue(start, end, function() {
			submitBooking(start, end, new Auth().getSessionId());
		});
	}

	function newBookingConfirmationDialogue(start, end, onConfirmCode) {
		pongUtil.confirmDialogue("Create new booking from " + dateUtil.formatTime(start) + " to " + dateUtil.formatTime(end) + "?", onConfirmCode);
	}
}