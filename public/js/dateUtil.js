function DateUtil() {

	this.addMinutes = function(date, minutes) {
		return new Date(date.getTime() + minutes * 60 * 1000);
	};

	this.getTodayByTime = function(hour, minutes) {
		var now = new Date();
		return new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour, minutes);
	};

	this.getMinutesBetween = function(start, end) {
		return Math.abs((end.getTime() - start.getTime()) / 60000).toFixed(0);
	};

	this.roundDownToFiveMinutes = function(date) {
		var result = copyDateWithFullMinutes(date);
		var mod = result.getMinutes() % 5;
		return this.addMinutes(result, -mod);
	};

	this.roundUpToFiveMinutes = function(date) {
		var result = copyDateWithFullMinutes(date);
		var mod = result.getMinutes() % 5;
		if (mod == 0) {
			return result;
		}
		return this.addMinutes(result, 5 - mod);
	};

	this.formatTime = function(date) {
		return this.padWithZero(date.getHours()) + ":" + this.padWithZero(date.getMinutes());
	};

	this.formatTimeInterval = function(from, to) {
		return this.formatTime(from) + " - " + this.formatTime(to);
	};

	this.padWithZero = function(number) {
		if (number >= 10 || number < 0) {
			return "" + number;
		}
		return "0" + number;
	};

	this.compareByDate = function(date1, date2) {
		var result = date1.getFullYear() - date2.getFullYear();
		if (result == 0) {
			result = date1.getMonth() - date2.getMonth();
			if (result == 0) {
				result = date1.getDate() - date2.getDate();
			}
		}
		return result;
	};

	function copyDateWithFullMinutes(date) {
		var result = new Date(date.getTime());
		result.setSeconds(0);
		result.setMilliseconds(0);
		return result;
	}
}

dateUtil = new DateUtil();