function Auth(config) {
    this.config = config;

    this.init = function() {
        initializeSession();
        $(config.loginButton).click(login);
        $(config.logoutButton).click(logout);
        loginOnEnterPress();
        pongUtil.callAndSetInterval(refreshDisplay, 60);
    };

    this.getSessionId = function() {
        return Cookies.get('sessionId');
    };

    function initializeSession() {
        var sessionId = pongUtil.getParameterByName('sessionId');
        if (sessionId)
            Cookies.set('sessionId', sessionId, {expires: 365});
    }

    function login() {
        var username = $(config.usernameInputField).val();

        // TODO: validate username
        if (username) {
            $.post("/v1/login", {userName: username})
                .done(function(data) {
                    console.log("login success:\n" + JSON.stringify(data));
                    email = username + '@netsuite.com';
                    pongUtil.showMessage('Log in link has been sent to ' + email, 'info');
                })
                .fail(function(data) {
                    console.log("login error:\n" + JSON.stringify(data));
                    pongUtil.showMessage('Error: ' + JSON.stringify(data));
                });
        } else {
            pongUtil.showMessage('NetSuite username is required', 'error');
        }
    }

    function logout() {
        Cookies.remove('sessionId');
        window.location = '/';
    }

    function refreshDisplay() {
        validateSession(function(isValidSession, username) {
            $(config.logoutForm).toggle(isValidSession);
            $(config.loginForm).toggle(!isValidSession);

            if (isValidSession)
                $(config.loggedInUserContainer).text(username);
            else if (Cookies.get('sessionId'))
                pongUtil.showMessage('Session validation has failed', 'error', logout);

            enableOrDisableNewBookingButton(isValidSession, username);
        });
    }

    function enableOrDisableNewBookingButton(isValidSession) {
        // TODO: if not valid session, disable on-click event + adding bookings using the calendar
        $(config.newBookingButton)
            .tooltip(isValidSession ? 'disable' : 'enable')
            .toggleClass('disabled', !isValidSession);
    }

    function validateSession(handler) {
        var sessionId = Cookies.get('sessionId');

        if (!sessionId) {
            handler(false);
        }
        else {
            $.post("/v1/login/validateSession/", {sessionId: sessionId})
                .done(function(data) {
                    console.log("session validation success:\n" + JSON.stringify(data));
                    handler(true, data.userName);
                })
                .fail(function(data) {
                    console.log("session validation error:\n" + JSON.stringify(data));
                    handler(false);
                });
        }
    }

    function loginOnEnterPress() {
        $(config.usernameInputField).keypress(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                $(config.loginButton).click();
            }
        });
    }
}